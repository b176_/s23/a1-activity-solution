// add 5 avengers into the users collection
db.users.insertMany([
    {
        "firstName": "Iron",
        "lastName": "Man",
        "email": "ironman@email.com",
        "password": "password123",
        "isAdmin": false
    },
    {
        "firstName": "Spider",
        "lastName": "Man",
        "email": "spiderman@email.com",
        "password": "password321",
        "isAdmin": false
    },
    {
        "firstName": "Captain",
        "lastName": "America",
        "email": "captainamerica@email.com",
        "password": "password231",
        "isAdmin": false
    },
    {
        "firstName": "The",
        "lastName": "Hulk",
        "email": "thehulk@email.com",
        "password": "password213",
        "isAdmin": false
    },
    {
        "firstName": "Black",
        "lastName": "Widow",
        "email": "blackwidow@email.com",
        "password": "password312",
        "isAdmin": false
    },
])

// add 3 courses into the courses collection
db.courses.insertMany([
    {
        "name": "Basic HTML and CSS",
        "price": 2000,
        "isActive": false
    },
    {
        "name": "Basic JavaScript",
        "price": 3500,
        "isActive": false
    },
    {
        "name": "Advanced JavaScript",
        "price": 5000,
        "isActive": false
    }
])

// find all instances of users where isAdmin is false
db.users.find({"isAdmin": false})



// update first user's isAdmin field to true
db.users.updateOne(
    {},
    {
        $set: {
            "isAdmin": true 
        }
    }
)

// update course with the name field of "Basic JavaScript" to have an isActive value of true
db.courses.updateOne(
    {
        "name": "Basic JavaScript"
    },
    {
        $set: {
            "isActive": true 
        }
    }
)

// delete all courses with isActive field valued false
db.courses.deleteMany(
    {
        "isActive": false
    }
)